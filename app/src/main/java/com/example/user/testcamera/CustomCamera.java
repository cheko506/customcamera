package com.example.user.testcamera;

import android.app.Fragment;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

public class CustomCamera extends Fragment implements SurfaceHolder.Callback, View.OnClickListener{

    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private Camera mCamera;
    private boolean mPreviewRunning = false;
    private Button btnTakePhoto;
    private Button btnCancelarFoto;
    private Button btnAceptarFoto;
    private byte[] data;

    Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {

        public void onPictureTaken(byte[] imageData, Camera c) {
            Log.e("CAMERA", "A PICTURE WAS TAKEN");
            Camera.Parameters p = mCamera.getParameters();
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                mCamera.setDisplayOrientation(90);
            }else{
                mCamera.setDisplayOrientation(180);

            }
            mCamera.setParameters(p);
            data = imageData;
            btnAceptarFoto.setVisibility(View.VISIBLE);
            btnCancelarFoto.setVisibility(View.VISIBLE);
            btnTakePhoto.setVisibility(View.GONE);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_custom_camera, container, false);
        btnAceptarFoto = (Button) v.findViewById(R.id.btnAceptarFoto);
        btnCancelarFoto = (Button) v.findViewById(R.id.btnCancelarFoto);
        btnAceptarFoto.setOnClickListener(this);
        btnCancelarFoto.setOnClickListener(this);
        btnTakePhoto = (Button) v.findViewById(R.id.btnTakePhoto);
        btnTakePhoto.setOnClickListener(this);
        mSurfaceView = (SurfaceView) v.findViewById(R.id.surface_camera);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        return v;
    }

    public Bitmap rotateBitmap(Bitmap bmp, int degrees){
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp,bmp.getWidth(),bmp.getHeight(),true);
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);

        return rotatedBitmap;
    }

    private void releaseCameraAndPreview() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mPreviewRunning = false;
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        releaseCameraAndPreview();
        mCamera = Camera.open();
        mCamera.startPreview();
        //STEP #1: Get rotation degrees

        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);
        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break; //Natural orientation
            case Surface.ROTATION_90: degrees = 90; break; //Landscape left
            case Surface.ROTATION_180: degrees = 180; break;//Upside down
            case Surface.ROTATION_270: degrees = 270; break;//Landscape right
        }
        int rotate = (info.orientation - degrees + 360) % 360;

//STEP #2: Set the 'rotation' parameter
        Camera.Parameters params = mCamera.getParameters();
        params.setRotation(rotate);
        mCamera.setParameters(params);
        mCamera.setDisplayOrientation(degrees);

        btnTakePhoto.setVisibility(View.VISIBLE);
        btnCancelarFoto.setVisibility(View.GONE);
        btnAceptarFoto.setVisibility(View.GONE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        releaseCameraAndPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if(mPreviewRunning){
            mCamera.stopPreview();
        }
        Camera.Parameters p = mCamera.getParameters();

        p.setPreviewSize(width, height);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mCamera.setDisplayOrientation(90);
        }else{
            mCamera.setDisplayOrientation(180);
        }

        mCamera.setParameters(p);
        try {

            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCamera.startPreview();
        mPreviewRunning = true;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
       releaseCameraAndPreview();
        ((MainActivity) getActivity()).getFragmentManager().beginTransaction().remove(this).commit();
        ((MainActivity)getActivity()).fragmentCamera.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnTakePhoto:
                mCamera.takePicture(null, null, mPictureCallback);

                break;

            case R.id.btnCancelarFoto:
                btnTakePhoto.setVisibility(View.VISIBLE);
                btnAceptarFoto.setVisibility(View.GONE);
                btnCancelarFoto.setVisibility(View.GONE);
                mCamera.startPreview();
                break;

            case R.id.btnAceptarFoto:
                Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                ((MainActivity) getActivity()).mostrarFoto.setImageBitmap(bmp);
                saveImage(data);
                ((MainActivity) getActivity()).getFragmentManager().beginTransaction().remove(this).commit();
                ((MainActivity)getActivity()).fragmentCamera.setVisibility(View.GONE);

                break;
        }
    }

    private void saveImage(byte[] dataArray){
        try {
            Bitmap image = BitmapFactory.decodeByteArray(dataArray, 0, dataArray.length);
            Date d = new Date();
            CharSequence s  = DateFormat.format("MM-dd-yy-hh-mm-ss", d.getTime());
            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath()+"/IMG_"+s.toString()+".jpg";
            OutputStream fOut = new FileOutputStream(path);
            long time = System.currentTimeMillis();
            image.compress(Bitmap.CompressFormat.JPEG,
                    100, fOut);
            System.out.println(System.currentTimeMillis() - time);
            fOut.flush();
            fOut.close();
            Toast.makeText(getActivity(), "Foto guardada", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Hubo un error al guardar la foto", Toast.LENGTH_SHORT).show();
        }

    }
}
