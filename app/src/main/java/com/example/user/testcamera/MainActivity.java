package com.example.user.testcamera;


import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnCamera;
    protected ImageView mostrarFoto;
    protected FrameLayout fragmentCamera;
    private CustomCamera cc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        referencias();

        Bundle b = getIntent().getExtras();
        if(b != null){
            if(b.get("data") != null){
                byte[] data = (byte[]) b.get("data");
                mostrarFoto.setImageBitmap(BitmapFactory.decodeByteArray(data, 0,data.length));
            }
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        referencias();

        Bundle b = getIntent().getExtras();
        if(b != null){
            if(b.get("data") != null){
                byte[] data = (byte[]) b.get("data");
                mostrarFoto.setImageBitmap(BitmapFactory.decodeByteArray(data, 0,data.length));
            }
        }
    }

    public void referencias(){
        btnCamera = (Button) findViewById(R.id.btnFoto);
        btnCamera.setOnClickListener(this);
        mostrarFoto = (ImageView) findViewById(R.id.ivMostrarFoto);
        fragmentCamera = (FrameLayout) findViewById(R.id.FragmentCamera);
    }

    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnFoto){
            if(checkCameraHardware(this)){
                fragmentCamera.setVisibility(View.VISIBLE);
                cc = new CustomCamera();
                android.app.FragmentManager manager = getFragmentManager();
                android.app.FragmentTransaction ft = manager.beginTransaction();
                ft.add(R.id.FragmentCamera, cc);
                ft.commit();
            }else{
                //No tiene cámara
                Toast.makeText(getApplicationContext(), "No tienes camara", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(cc.isVisible() && cc != null){
            getFragmentManager().beginTransaction().remove(cc).commit();
            fragmentCamera.setVisibility(View.GONE);
        }else{
            super.onBackPressed();
        }
    }
}
